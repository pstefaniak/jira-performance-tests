# Providing feedback on JPT

Thank you for taking the time to provide feedback on JPT.

Issues are tracked in the [public Ecosystem Jira instance][ecosystem-jira],
in the Jira Performance Testing Tools (JPERF) project.

## Raising bugs

Before you raise a bug, take a look at the [known bug list][bug-list].

If you find it, vote on it and watch it. Add comments if it's missing
some important context, or [contribute][article-contributing] a solution.
We'll provide further updates in the ticket.

If you don't find it, [create a new bug][bug-create]. When you create a
new bug, please include the following information:

* description of the problem,
* requirements and steps to reproduce the issue,
* expected behavior,
* actual behavior,
* attach the `target/jpt-workspace/jpt.log` log file.

## Adding suggestions

Before you share a suggestion, take a look at the
[existing suggestions list][suggestion-list].

If you find it, vote on it and watch it. We'll provide further updates
in the ticket.

If you don't find it, [create a new suggestion][suggestion-create]. When
you create a new suggestion, make sure you describe:

* a problem you would like to solve,
* optionally provide an example,
* optionally provide suggestions on how you think it should be solved.

## Feedback

Atlassian is full of crazy people and [Jakub Łaziński][mailto-jlazinski],
the lead Product Manager on JPT, happens to be just crazy enough to put
his email address in a public doc. He'd love to hear about your perception
of JPT, successes, hardships and opinions. Drop him an email!

[article-contributing]: CONTRIBUTING.md
[ecosystem-jira]: https://ecosystem.atlassian.net/secure/RapidBoard.jspa?rapidView=457&projectKey=JPERF&view=planning
[suggestion-list]: https://ecosystem.atlassian.net/issues/?filter=61606
[suggestion-create]: https://ecosystem.atlassian.net/secure/CreateIssue!default.jspa?projectKey=JPERF&issuetype=11500
[bug-list]: https://ecosystem.atlassian.net/issues/?filter=61607
[bug-create]: https://ecosystem.atlassian.net/secure/CreateIssue\!default.jspa?projectKey=JPERF&issuetype=1
[mailto-jlazinski]: mailto:jlazinski@atlassian.com?subject=JPT%20Feedback